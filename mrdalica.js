const robot = require('robotjs')
const readline = require('readline');
const iohook = require('iohook');

const printHeader = require('./utils').printHeader

/**
 * Start hooks for keypress
 */
iohook.on("keydown", event => {
    if (event.keycode === 18) {
        console.log('\nExiting on demand....')
        process.exit(0)
    }
});
iohook.start();

/**
 * Initialize read line interface for console input.
 */
const readLineInterface = readline.createInterface({
    input: process.stdin,
    output: process.stdout
});

/**
 * Config
 * */
const RADIUS = 200
const X_START = 700
const Y_START = 500
const START_IN = 10000
const SPEED = 0.01

/**
 * Intro
 * */
printHeader()

/**
 * Feature
 * */
const move = () => {
    const angles = [...Array(360).keys()]
    for (const angle in angles) {
        let x = X_START
        let y = Y_START
        const radAngle = angle * 0.0174532925
        x += Math.round(RADIUS * Math.cos(radAngle))
        y += Math.round(RADIUS * Math.sin(radAngle))
        robot.moveMouseSmooth(x, y, SPEED)
    }
}

const moveHandler = (startTime, minutes) => {
    process.stdout.write('.')
    move()

    const endTime = new Date()
    let timeDiff = endTime - startTime
    timeDiff = timeDiff / 1000

    if (timeDiff < minutes * 60) {
        return setTimeout(() => moveHandler(startTime, minutes), 10)
    }

    const timeDiffInMinutes = Math.round(timeDiff / 60)
    console.log('\n======================================================')
    console.log('\nProcess finished!')
    console.log(`Total working time: ${timeDiffInMinutes} minutes.`)
    iohook.stop()
    process.exit(0)
}

const init = () => {
    const START_TIME = new Date()
    console.log(`Script will start in ${START_IN / 1000} seconds...`)
    console.log('Press (e) to exit at any time.')
    setTimeout(() => moveHandler(START_TIME, MINUTES), START_IN)
}

/**
 * Start script
 */
let MINUTES = 1

readLineInterface.question('Enter number of minutes (1, 2, 60, 320...): ', (minuteAnswer) => {
    if (minuteAnswer) {
        MINUTES = parseInt(minuteAnswer)
        init()
    } else {
        console.log('Input is missing.')
        console.log(`Setting default value for process running: ${MINUTES} minute.`)
        init()
    }
    readLineInterface.close()
})
